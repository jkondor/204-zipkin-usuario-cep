package br.com.zipkin.usercep;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
public class UserCepControler {

    @Autowired
    private UserCepFeign userCepFeign;

    @GetMapping("/usercep/{usuario}/{cep}")
    public HashMap<String, Object> buscar(@PathVariable String usuario ,@PathVariable String cep) {

        HashMap<String, Object> hashMap =  userCepFeign.buscarCep(cep);
        hashMap.put("usuario", usuario);

        return hashMap;
    }
}
