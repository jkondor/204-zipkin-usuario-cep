package br.com.zipkin.usercep;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.HashMap;

@FeignClient(name = "ViaCep")
public interface UserCepFeign {

    @GetMapping("/viacep/{cep}")
    HashMap<String, Object> buscarCep(@PathVariable String cep);
}
