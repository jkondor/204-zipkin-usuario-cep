package br.com.zipkin.usercep;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class UsercepApplication {

	public static void main(String[] args) {
		SpringApplication.run(UsercepApplication.class, args);
	}

}
